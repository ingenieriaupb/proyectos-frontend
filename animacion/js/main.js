var cubos = [];
var timeline;
var boton;

function propiedades(){
	var dist = [];
	for(var i=0;i<cubos.length;i++)
	{
		dist.push({t:Math.random()*3+1,a:Math.random()*360});
	}
	return dist;
}
function animacion(){
	var props = propiedades();
	for(var i in cubos)
	{
		timeline.add(TweenLite.to(cubos[i],props[i].t,{left:1000,rotation:props[i].a}));
	}
}
function empezarAnimacion(event){
	timeline.play();
}
window.onload = function(){
	boton = document.getElementById("boton");
	timeline = new TimelineLite({paused:true});
	cubos.push(document.getElementById("cubo_1"));
	cubos.push(document.getElementById("cubo_2"));
	cubos.push(document.getElementById("cubo_3"));
	cubos.push(document.getElementById("cubo_4"));
	boton.addEventListener("click",empezarAnimacion);
	animacion();
}