module.exports = function(grunt) {

  //Configuración de las tareas que estarán disponibles para usar con el comando grunt
  grunt.initConfig({
  	clean: {
  		js: ["js/*.js", "!js/*.min.js"]
	},
	concat: {
		options: {
			separator: ';',
		},
		dist: {
		  src: ['js/com/Datos.js', 'js/com/Usuario.js', 'js/com/Utilidades.js'],
		  dest: 'dist/built.js',
		}
	},
	uglify: {
		my_target: {
		  files: {
		    'dist/built.min.js': ['dist/built.js']
		  }
		}
	}
  });
  //Carga de la tarea, que se va a usar
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  //Registrar la tarea, para poderla usar
  //La tarea por defecto es la que se ejecuta al digitar
  //el comando grunt
   grunt.registerTask('default', ['clean','concat','uglify']);
   //Se define una tarea personalizada, se ejecuta con grunt concatenar
   grunt.registerTask('concatenar', ['concat']);
}