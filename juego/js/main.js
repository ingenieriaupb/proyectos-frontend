var tabs;
var secciones;

function desordenar(input){
	for (var i = input.length-1; i >=0; i--) {
	
		var randomIndex = Math.floor(Math.random()*(i+1)); 
		var itemAtIndex = input[randomIndex]; 
		
		input[randomIndex] = input[i]; 
		input[i] = itemAtIndex;
	}
	return input;
}
function repartirFichas(fichas_dom,fichas)
{
	for (var i = fichas.length - 1; i >= 0; i--) {
		fichas_dom[i].innerHTML = fichas[i];
	};
}

// background-color ->css 
// backgroundColor  ->js
function ocultar(secciones_temp){
	for(var i = 0; i< 3; i++){
		secciones_temp[i].style.display = 'none';
	}
}
function mostrarSeccion(seccion){
	seccion.style.display = "block";
}
function procesarClick(event){
	var seccion = (event.target.id + "").split("_")[0];
	var id_temp = document.getElementById(seccion);
	ocultar(secciones);
	mostrarSeccion(id_temp);
}
function crearMenu(tabs_temp){
	for(var i in tabs_temp){
		tabs_temp[i].addEventListener("click",procesarClick);
	}
}
window.onload = function(){
	var fichas_dom = document.getElementsByClassName("ficha");
	var fichas = [1,2,3,4,5,6,7,8];
	tabs = document.getElementsByClassName("tab");
	secciones = document.getElementsByClassName("seccion");
	crearMenu(tabs);
	fichas = desordenar(fichas);
	repartirFichas(fichas_dom,fichas);
}